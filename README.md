# Projet Web Services API Authentification

## 🤝 Contexte du Projet
>Ce projet est réalisé en collaboration avec **AMARA Melyssa** et **BANNOUF Abdessamad** en mode pair programming, dans le cadre d'un cours sur les Web Services. L'objectif était de développer une API dédiée à un système d'authentication pour les Api Movies et Réservations.

## 🛠 Prérequis

Avant de démarrer, assurez-vous d'avoir installé les outils nécessaires :
- [Docker](https://www.docker.com/) - Pour la conteneurisation de l'application.
- [Docker Compose](https://docs.docker.com/compose/) - Pour la gestion multi-conteneurs.

## 🔧 Installation et Lancement

### 🗃️ Clonage du Projet
Clonez le dépôt Git pour récupérer le projet :
```sh
https://gitlab.com/CalixteManaud/webservice-authentification-2.0.git
```

### 🔑 Génération de la clé laravel
Accéder au projet et générer la clé laravel avec la commande suivante :
    
```sh
php artisan key:generate
```

## 🔑 Génération de la clé

Avant de lancer Docker, générez vos clés :

```sh
openssl genpkey -algorithm RSA -out private_key.pem -pkeyopt rsa_keygen_bits:2048
openssl rsa -pubout -in private_key.pem -out public_key.pem
```
Convertir les clés en base64
```sh
base64 private_key.pem > private_key_base64.pem
base64 public_key.pem > public_key_base64.pem
```
Mettre la valeur des clés dans le fichier .env
```.dotenv
PRIVATE_KEY=$(< private_key_base64.pem)
PUBLIC_KEY=$(< public_key_base64.pem)
```

### 🚜 Construction et Lancement des Conteneurs

Construisez et lancez les conteneurs Docker à l'aide de :

```sh
docker-compose build
docker-compose up -d
```
### 📶 Création d'un Réseau pour les Conteneurs

Créez un réseau Docker pour permettre la communication entre les conteneurs :

```sh
docker network create app-network
```
### 📝 Configuration Environnementale

Copiez le contenu de `.env.example` dans un nouveau fichier `.env` et ajustez les paramètres de connexion à la base de données selon vos besoins :

```makefile
DB_DATABASE=VotreChoix
DB_USERNAME=VotreChoix
DB_PASSWORD=VotreChoix
```

Si pas de dossier vendor, il faut installer les dépendances avec la commande suivante :

```sh
composer u
```

## 🌐 Accès au Projet

Naviguez vers `http://localhost:7040` pour accéder à l'interface du projet.

### Migrations et Seeders

Si nécessaire, effectuez les migrations et seeders avec :

```sh
docker-compose exec auth-app php artisan migrate
```

## 📦 Accès aux Outils

Base de Données via PHPMyAdmin
Accédez à PHPMyAdmin via http://localhost:8156 avec les identifiants suivants :

```yaml
Identifiant : laravel
Mot de passe : laravel
```

## API via Scramble

Vous pouvez tester les fonctionnalités de l'API avec Scramble. En allant sur le lien suivant : http://localhost:7040/docs/api
Vous pourrez tester les différentes routes de l'API.

## Fonctionnalités : POSTMAN

Vous pouvez tester les fonctionnalités de l'API avec POSTMAN. Pour explorer l'API, utilisez `http://localhost:7040/api`.

## Créer un compte

Pour créer un compte, il faut envoyer une requête POST à l'adresse suivante : http://localhost:7040/api/register

Il faut envoyer les données suivantes :

```json
{
    "name": "John Doe",
    "email": "xxxxx@gmail.com",
    "password": "xxxxx",
    "role" : "ROLE_USER or ROLE_ADMIN"
}
```

## Se connecter

Pour se connecter, il faut envoyer une requête POST à l'adresse suivante : http://localhost:7040/api/login

Il faut envoyer les données suivantes :

```json
{
    "email": "xxxxx@gmail.com",
    "password": "xxxxx"
}
```

## Récupérer les informations de l'utilisateur connecté

Pour récupérer les informations de l'utilisateur connecté, il faut envoyer une requête GET à l'adresse suivante : http://localhost:7040/api/view
Il faut envoyer le token dans le header de la requête.

## Modifier les informations de l'utilisateur connecté

Pour modifier les informations de l'utilisateur connecté, il faut envoyer une requête PUT à l'adresse suivante : http://localhost:7040/api/update
Il faut envoyer le token dans le header de la requête.

Il faut envoyer les données suivantes :

```json
{
    "name": "John Doe",
    "email": "xxxxx@gmail.com",
    "password": "xxxxx",
    "role" : "ROLE_USER or ROLE_ADMIN"
}
```

## Rafraîchir le token

Pour rafraîchir le token, il faut envoyer une requête POST à l'adresse suivante : http://localhost:7040/api/refresh-token
Il faut envoyer le token dans le header de la requête.

## Valider le token

Pour valider le token, il faut envoyer une requête POST à l'adresse suivante : http://localhost:7040/api/valid
Il faut envoyer le token dans le header de la requête.
