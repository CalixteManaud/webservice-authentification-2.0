<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use DateTimeImmutable;
use Random\RandomException;
use Symfony\Component\Routing\Attribute\Route;

class AuthController extends Controller
{
    /**
     * Permet de lister les utilisateurs
     * @unauthenticated
     * @Route("/users", methods={"GET"})
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        try {
            $users = User::all();

            if ($users->isEmpty())
                return response()->json(['message' => 'Aucun utilisateur trouvé'], 404);

            $userData = $users->map(function ($user) {
                return [
                    'uid' => $user->id,
                    'email' => $user->email,
                    'role' => $user->role,
                    'created_at' => $user->created_at,
                    'updated_at' => $user->updated_at,
                ];
            });

            return response()->json($userData, 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Erreur interne du serveur'], 500);
        }
    }

    /**
     * Permet de créer un utilisateur
     * @unauthenticated
     * @Route("/register", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'role' => 'required|string|max:255|in:ROLE_ADMIN,ROLE_USER',
            'statut' => 'string|max:255|in:open,closed|nullable',
        ]);

        if ($validator->fails())
            return response()->json($validator->errors(), 400);


        $user = User::create([
            "name" => $request->name,
            "email" => $request->email,
            "role" => $request->role,
            "statut" => $request->statut ? $request->statut : "open",
            "password" => Hash::make($request->password), // Hashage du mot de passe
        ]);

        return response()->json([
            "uid" => $user->id,
            'email' => $user->email,
            "role" => $user->role,
            "created_at" => $user->created_at,
            "updated_at" => $user->updated_at,
        ], 201);
    }

    /**
     * Permet de se connecter
     * @unauthenticated
     * @Route("/login", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws RandomException
     */
    public function login(Request $request) : JsonResponse {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails())
            return response()->json($validator->errors(), 400);

        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password))
            return response()->json(['error' => 'Email or password is false'], 401);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            [$accessToken, $accessTokenExpiration] = $this->createJwt($user->id, 120); // 60 minutes pour l'access token
            [$refreshToken, $refreshTokenExpiration] = $this->createJwt($user->id, 180); // 120 minutes pour le refresh token
            Auth::login($user);
        }

        return response()->json([
            'access_token' => $accessToken,
            'access_token_expiration' => $accessTokenExpiration,
            'refresh_token' => $refreshToken,
            'refresh_token_expiration' => $refreshTokenExpiration
        ]);
    }

    /**
     * Créer un token JWT
     * @param string $userId
     * @param int $minutesToExpire
     * @return array
     * @throws RandomException
     * @throws Exception
     */
    private function createJwt(string $userId, int $minutesToExpire): array {
        $issuedAt = new DateTimeImmutable();

        $user = User::find($userId);

        if (!$user)
            throw new Exception('User not found');

        $data = [
            'iat' => $issuedAt->getTimestamp(),
            'jti' => base64_encode(random_bytes(16)),
            'iss' => env("APP_URL"),       // Issuer
            'nbf' => $issuedAt->getTimestamp(), // Not before
            'exp' => $issuedAt->modify("+$minutesToExpire minutes")->getTimestamp(), // Expire
            'data' => [
                'userId' => $userId,
                'role' => $user->role,
                'name' => $user->name,
                ],
            'uid' => $userId,
        ];

        $jwt = JWT::encode($data, base64_decode(env("PRIVATE_KEY")), 'HS512');
        return [$jwt, $issuedAt->modify("+$minutesToExpire minutes")->format('Y-m-d H:i:s')];
    }

    /**
     * Permet de consulter un compte
     * @Route("/account/{id}", methods={"GET"})
     * @param int $id
     * @return JsonResponse
     */
    public function view(int $id) : JsonResponse {
        $user = request()->auth;
        if (!$user)
            return response()->json(['error' => 'Aucun utilisateur authentifier'], 401);

        if ($id === "me" || $id == $user->id || $user->isAdmin()) {
            $id = $id === "me" ? $user->id : $id;

            $user = User::findOrFail($id);

            return response()->json([
                "uid" => $user->id,
                'email' => $user->email,
                "role" => $user->role,
                "created_at" => $user->created_at,
                "updated_at" => $user->updated_at,
            ], 200);
        } else {
            return response()->json(['error' => 'Vous n\'avez pas les droits pour consulter ce compte'], 403);
        }
    }

    /**
     * Permet de mettre à jour un compte
     * @param Request $request
     * @param mixed $id Identifiant du compte à modifier
     * @return JsonResponse
     */
    public function update(Request $request, mixed $id): JsonResponse
    {
        try {
            $authUser = request()->auth; // Assurez-vous que cette ligne récupère bien l'utilisateur authentifié

            // Vérifier si l'utilisateur est autorisé à modifier le compte
            if ($id !== "me" && $id != $authUser->id && !$authUser->isAdmin()) {
                return response()->json([
                    'error' => 'Vous devez être administrateur pour modifier un autre compte.'
                ], 403);
            }

            $validatedData = $request->validate([
                'name' => 'string|max:255',
                'email' => 'email|unique:users',
                'password' => 'min:8|nullable',
                'role' => 'string|max:255|in:ROLE_ADMIN,ROLE_USER',
                'status' => 'string|max:255|in:open,closed',
            ]);

            // Identifier le compte à modifier
            $user = $id === "me" ? $authUser : User::findOrFail($id);

            // Seul un admin peut modifier les rôles et le statut
            if (($request->has('role') || $request->has('status')) && !$authUser->isAdmin()) {
                return response()->json([
                    'error' => 'Vous devez être administrateur pour modifier le rôle ou le statut.'
                ], 403);
            }

            // Mettre à jour le compte avec les données validées, à l'exception du mot de passe et du rôle
            $user->fill(Arr::except($validatedData, ['password', 'role', 'status']));

            // Changer le mot de passe si fourni et validé
            if ($request->has('password')) {
                $user->password = Hash::make($validatedData['password']);
            }

            // Changer le rôle et le statut si fournis et validés et si l'utilisateur est admin
            if ($authUser->isAdmin()) {
                if ($request->has('role')) {
                    $user->role = $validatedData['role'];
                }
                if ($request->has('status')) {
                    $user->status = $validatedData['status'];
                }
            }

            $user->save();

            return response()->json($user, 200);
        } catch (\Exception $e) {
            // Retourne une réponse JSON en cas d'exception
            return response()->json(['error' => 'Une erreur est survenue: ' . $e->getMessage()], 500);
        }
    }


    /**
     * Permet de rafraîchir un token
     * @Route("/refresh", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function refreshToken(Request $request): JsonResponse
    {
        $token = $request->bearerToken();

        try {
            $decoded = JWT::decode($token, new Key(base64_decode(env("PRIVATE_KEY")), 'HS512'));
            if (isset($decoded->data->userId)) {
                $credentials = User::find($decoded->data->userId);

                if (is_null($credentials))
                    return response()->json(['error' => 'User not found'], 404);
            } else
                return response()->json(['error' => 'User ID not included in token'], 400);

            [$accessToken, $accessTokenExpiration] = $this->createJwt($decoded->data->userId, 120); // 60 minutes pour l'access token
            [$refreshToken, $refreshTokenExpiration] = $this->createJwt($decoded->data->userId, 180); // 120 minutes pour le refresh token

            return response()->json([
                'access_token' => $accessToken,
                'access_token_expiration' => $accessTokenExpiration,
                'refresh_token' => $refreshToken,
                'refresh_token_expiration' => $refreshTokenExpiration
            ]);
        } catch (\Exception $e) {
            Log::error('JWT error: ' . $e->getMessage());
            return response()->json(["message 2" => $e->getMessage()], 401);
        }
    }

    /**
     * Permet de valider un token
     * @param Request $request token
     * @return JsonResponse
     */
    public function validateToken(Request $request): JsonResponse
    {
        $tokenDetails = $request->attributes->get('tokenDetails');

        return response()->json([
            'message' => 'Token validé',
            'tokenDetails' => $tokenDetails,
        ]);
    }
}
