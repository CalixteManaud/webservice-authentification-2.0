<?php

namespace App\Http\Middleware;

use Closure;
use DateTimeImmutable;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class ValidateJwtToken
{
    public function handle(Request $request, Closure $next): JsonResponse
    {
        $token = $request->bearerToken();
        if (!$token) {
            return response()->json(['error' => 'Token not provided'], 401);
        }

        try {
            $decoded = JWT::decode($token, new Key(base64_decode(env("PRIVATE_KEY")), 'HS512'));
            $expirationDate = (new DateTimeImmutable())->setTimestamp($decoded->exp);
            $request->attributes->add([
                'tokenDetails' => [
                    'token' => $token,
                    'valid' => (new DateTimeImmutable() < $expirationDate),
                    'expiration' => $expirationDate->format('Y-m-d H:i:s'),
                ]
            ]);
            return $next($request);
        } catch (\Throwable $e) {
            Log::error('JWT error: ' . $e->getMessage());
            return response()->json(['error' => 'Invalid token'], 401);
        }
    }
}
