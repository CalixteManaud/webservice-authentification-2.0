<?php

namespace App\Helpers;

use App\Models\User;

class AdminHelper
{
    public function GetAuthUser()
    {
        $publicHelper = new PublicHelper();
        $token = $publicHelper->GetAndDecodeJWT();

        $userId = $token->data->id;
        $user = User::find($userId);

        return $user;
    }
}
