<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('register', [AuthController::class, 'register'])->middleware('throttle:10,1');
Route::post('login', [AuthController::class, 'login']);
Route::get('/users', [AuthController::class, 'index']);
Route::middleware('jwt')->group(function () {
    Route::put("/account/{id}", [AuthController::class, 'update']);
    Route::post('/refresh-token', [AuthController::class, 'refreshToken'])->middleware('throttle:10,1');
    Route::get("/account/{id}", [AuthController::class, 'view']);
    Route::get('/valid', [AuthController::class, 'validateToken'])->middleware('validateJwtToken');
});

